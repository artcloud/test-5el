import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = () => new Vuex.Store({

  modules: {
    mobile: {},
    desktop: {},
    mkp: {}
  },
  state: {
    isMobile: false,
    isDesktop: false,
    isMkp: false
  },
  mutations: {
    increment (state) {
      state.counter++
    }
  }
})

export default store
