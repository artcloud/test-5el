export default function (context) {
  if (process.server) {
    console.log(context.req.headers['user-agent'])
    if (context.req.headers['user-agent'].indexOf('mkp') > -1) {
      context.store.state.isMkp = true
    } else if (context.isDesktop) {
      context.store.state.isDesktop = true
    } else if (context.isMobileOrTablet) {
      context.store.state.isMobile = true
    }
  }
}
